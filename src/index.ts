import "reflect-metadata";
import "dotenv/config";
import { createConnection, getConnectionOptions } from "typeorm";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import graphqlHTTP from "express-graphql";
import cors from "cors";
import { AnswerResolver } from "./resolvers/AnswerResolver";

(async () => {
	const app = express();

	const port = process.env.PORT || 8080;

	const corsWhitelist = [
		`http://localhost:${port}`,
		"https://shouldyoudoit-qa.surge.sh",
		"https://shouldyoudoit.surge.sh/"
	];
	app.use(
		cors({
			origin: (origin, callback) => {
				if (corsWhitelist.indexOf(origin as string) !== -1 || !origin) {
					callback(null, true);
				} else {
					callback(new Error("Not allowed by CORS"));
				}
			}
		})
	);

	const options = await getConnectionOptions(process.env.NODE_ENV || "dev");
	await createConnection({ ...options, name: "default" });

	const graphqlSchema = await buildSchema({
		resolvers: [AnswerResolver],
		validate: true
	});

	const apolloServer = new ApolloServer({
		schema: graphqlSchema,
		context: ({ req, res }) => ({ req, res })
	});

	apolloServer.applyMiddleware({ app, cors: false });

	app.use(
		"/api",
		graphqlHTTP({
			schema: graphqlSchema,
			graphiql: true
		})
	);

	app.listen(port, () => {
		console.log(`Server started at http://localhost:${port}/graphql`);
		console.log(`Server started at http://localhost:${port}/api -> For HTTP Requests`);
	});
})();
