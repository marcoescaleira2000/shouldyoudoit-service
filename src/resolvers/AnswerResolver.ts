import { Resolver, Mutation, Arg, Int, Query } from "type-graphql";
import { Answer } from "../entity/Answer";
import { getRandomArrayPos } from "../utils/maths";

@Resolver()
export class AnswerResolver {
	/*
	 *  CREATE
	 */
	@Mutation(() => Boolean)
	async createAnswer(@Arg("message", () => String) message: string, @Arg("gif", () => String) gif: string) {
		try {
			await Answer.insert({
				message,
				gif
			});
			return true;
		} catch (error) {
			console.error("CREATE CARD: ", error);
		}
		return false;
	}

	/*
	 *  READ
	 */
	@Query(() => [Answer])
	answers() {
		return Answer.find();
	}

	@Query(() => Answer)
	async randomAnswer() {
		const allAnswers = await this.answers();
		const randomPos = getRandomArrayPos(allAnswers.length);

		return allAnswers[randomPos];
	}

	/*
	 *  UPDATE
	 */
	@Mutation(() => Boolean)
	async updateAnswer(
		@Arg("id", () => Int) id: number,
		@Arg("message", () => String) message: string,
		@Arg("gif", () => String) gif: string
	) {
		try {
			await Answer.update(
				{ id },
				{
					message,
					gif
				}
			);
			return true;
		} catch (error) {
			console.error("UPDATE CARD: ", error);
		}
		return false;
	}

	/*
	 *  DELETE
	 */
	@Mutation(() => Boolean)
	async deleteAnswer(@Arg("id", () => Int) id: number) {
		try {
			await Answer.delete({
				id
			});
			return true;
		} catch (error) {
			console.error("DELETE CARD: ", error);
		}
		return false;
	}
}
