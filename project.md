## Should You Do It app

An app where you ask anything and it will answer if you should do it or not.

## User Stories

 - Admin authentication
 - Answers CRUD
 - Get an random answer
 - Get all answers

## Stack

 - Database: PostgreSQL
 - REST API - Nest JS
 - GraphQL API - Nest JS